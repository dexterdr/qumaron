<?php

namespace Tests;

use App\Validator\TruckValidator;
use App\Validator\PassengerValidator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class CarValidatorTest extends TestCase
{
    /**
     * Passenger car validation test.
     *
     * @param array<mixed> $data
     * @param bool $isValid
     *
     * @dataProvider providerTestPassengerCarValidator
     */
    public function testPassengerCarValidator(array $data, bool $isValid): void
    {
        if (!$isValid) {
            $this->expectException(InvalidArgumentException::class);
        }

        $validator = new PassengerValidator();
        $prepared = $validator->validate($data);
        $this->assertArrayHasKey('title', $prepared);
    }

    /**
     * Only boundary value for this test.
     *
     * @return array<string, mixed>
     */
    public function providerTestPassengerCarValidator(): array
    {
        return [
            [[], false],
            [$this->getValidPassengerCarData(), true]
        ];
    }

    /**
     * Get valid passengers car data.
     *
     * @return array<string, mixed>
     */
    private function getValidPassengerCarData(): array
    {
        return [
            'title' => 'Car',
            'body' => 'SPORT',
            'engine' => 'PETROL',
            'volume' => 4.5,
            'transmission' => 'MANUAL',
            'gearbox' => 4,
            'color' => 'RED',
            'interior' => 'COMFORT',
        ];
    }

    /**
     * Test lorry car validator with valid passengers data.
     */
    public function testTruckCarValidator(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $validator = new TruckValidator();
        $validator->validate($this->getValidPassengerCarData());
    }
}
