<?php

namespace Tests;

use App\Entity\Customer;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{
    /**
     * Test for the customer validation class.
     *
     * @param string $name
     * @param string $email
     *
     * @dataProvider providerTestValidation
     */
    public function testValidation(string $name, $email): void
    {
        $this->expectException(InvalidArgumentException::class);

        new Customer($name, $email);
    }

    /**
     * Provider of invalid data for testValidator().
     *
     * @return array<array>
     */
    public function providerTestValidation(): array
    {
        return [
            ['name' => '   ', 'email' => 'test@example.org'],
            ['name' => 'Name', 'email' => 'test'],
            ['name' => 'Name', 'email' => []],
        ];
    }

    /**
     * Object instantiation and some getters testing.
     */
    public function testGetters(): void
    {
        $client = new Customer('Name', 'test@example.org');

        $this->assertEquals('Name', $client->getName());
        $this->assertEquals('test@example.org', $client->getEmail());
        $this->assertEquals([], $client->getOrders());
    }
}
