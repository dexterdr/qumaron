<?php

namespace Tests;

use App\Controller;
use Core\Container;
use PHPUnit\Framework\TestCase;

class BasicTest extends TestCase
{
    /**
     * Just to run application with clear container.
     */
    public function testBasic(): void
    {
        $title = 'My Car';

        $container = new Container();
        $container->set('request', [
            'Name' => [
                'email' => 'test@example.org',
                'orders' => [
                    $title => [
                        'type' => 'TRUCK',
                        'body' => 'HUGE',
                        'engine' => 'DIESEL',
                        'transmission' => 'MANUAL',
                        'color' => 'RED',
                        'interior' => 'LUX',
                        'volume' => 10,
                        'gearbox' => 6,
                    ],
                ],
            ],
        ]);

        ob_start();
            (new Controller())->execute($container);
        $output = ob_get_clean();

        $this->assertStringContainsString("*** {$title} ***", $output);
    }
}
