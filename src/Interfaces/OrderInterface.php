<?php

namespace App\Interfaces;

interface OrderInterface
{
    /**
     * Get order title.
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Get order type (TRUCK or AUTOMOBILE).
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Get order details.
     *
     * @return array<string, mixed>
     */
    public function getDetails(): array;

    /**
     * Update details after manager's review.
     *
     * @param array<string, mixed> $details
     */
    public function setDetails(array $details): void;
}
