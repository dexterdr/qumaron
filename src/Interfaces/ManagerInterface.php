<?php

namespace App\Interfaces;

use App\Entity\Car\AbstractCar;

interface ManagerInterface
{
    /**
     * Process all client's order.
     *
     * @param CustomerInterface $customer
     *
     * @return AbstractCar[]
     */
    public function processClient(CustomerInterface $customer): array;
}
