<?php

namespace App\Interfaces;

/**
 * Interface ClientInterface
 *
 * In the future, there may be some special types of customers.
 */
interface CustomerInterface
{
    /**
     * Get customer's name.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Get customer's email.
     *
     * @return string
     */
    public function getEmail(): string;

    /**
     * Set customer email.
     *
     * @param string $email
     */
    public function setEmail($email): void;

    /**
     * Create a new order for the customer.
     *
     * @param string $title
     * @param array<string, mixed> $details
     */
    public function addOrder(string $title, $details): void;

    /**
     * Get array with orders.
     *
     * @return OrderInterface[]
     */
    public function getOrders(): array;
}
