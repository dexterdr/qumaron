<?php

namespace App\Interfaces;

use App\Entity\Car\AbstractCar;
use App\Entity\Engine\AbstractEngine;
use App\Entity\Stage\AbstractHandler;
use Core\Interfaces\ValidatorInterface;

interface FactoryInterface
{
    /**
     * Get validator for specific car type.
     *
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface;

    /**
     * Create specific car.
     *
     * @param string $name
     *
     * @return AbstractCar
     */
    public function createCar(string $name): AbstractCar;

    /**
     * Create specific Engine.
     *
     * @param string $type
     * @param float $volume
     *
     * @return AbstractEngine
     */
    public function createEngine(string $type, float $volume): AbstractEngine;

    /**
     * Get assembly chain.
     *
     * @return AbstractHandler
     */
    public function getAssemblyChain(): AbstractHandler;
}
