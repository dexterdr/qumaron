<?php

namespace App\Validator;

final class PassengerValidator extends CarValidator
{
    protected const BODY = ['SPORT', 'SEDAN', 'MINIBUS'];
    protected const VOLUME_MIN = 1;
    protected const VOLUME_MAX = 5;
}
