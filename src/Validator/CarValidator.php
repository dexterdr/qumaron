<?php

namespace App\Validator;

use Core\Interfaces\ValidatorInterface;
use InvalidArgumentException;

abstract class CarValidator implements ValidatorInterface
{
    protected const BODY = [];
    protected const ENGINE = ['DIESEL', 'PETROL'];
    protected const VOLUME_MIN = 1;
    protected const VOLUME_MAX = 10;
    protected const TRANSMISSION = ['MANUAL', 'AUTOMATIC'];
    protected const GEARBOX_MIN = 3;
    protected const GEARBOX_MAX = 6;
    protected const COLORS = ['RED', 'GREEN', 'BLUE'];
    protected const INTERIOR = ['MINIMAL', 'COMFORT', 'LUX'];
    protected const OPTIONS = ['NAVIGATOR', 'MUSIC', 'CONDITIONER'];

    /**
     * @inheritDoc
     */
    public function validate(array $data): array
    {
        $this->ruleInArray('body', $data, static::BODY);
        $this->ruleInArray('engine', $data, self::ENGINE);
        $this->ruleInArray('transmission', $data, self::TRANSMISSION);
        $this->ruleInArray('color', $data, self::COLORS);
        $this->ruleInArray('interior', $data, self::INTERIOR);

        $data['volume'] = (float)($data['volume'] ?? 0.0);
        $this->ruleInRange('volume', $data['volume'], static::VOLUME_MIN, static::VOLUME_MAX);
        $data['gearbox'] = (int)($data['gearbox'] ?? 0);
        $this->ruleInRange('gearbox', $data['gearbox'], self::GEARBOX_MIN, self::GEARBOX_MAX);

        $data['options'] = $this->processOptions($data['options'] ?? []);

        return $data;
    }

    /**
     * Additional car options processing.
     *
     * @param mixed $options
     *
     * @return array<string>
     */
    protected function processOptions($options): array
    {
        if (!is_array($options)) {
            throw new InvalidArgumentException('Variable `options` must be an array!');
        }

        foreach ($options as $key => $value) {
            $options[$key] = is_string($value) ? mb_strtoupper(trim($value)) : $value;
            if (!in_array($value, self::OPTIONS, true)) {
                unset($options[$key]);
            }
        }

        return $options;
    }

    /**
     * Test some parameter with in_array() function.
     *
     * @param string $key
     * @param array<string, mixed> $source
     * @param array<mixed> $available
     */
    protected function ruleInArray(string $key, array &$source, array $available): void
    {
        $value = $source[$key] ?? null;
        if (is_string($value)) {
            $source[$key] = mb_strtoupper($value);
        }

        if (!isset($source[$key]) || !in_array($source[$key], $available, true)) {
            throw new InvalidArgumentException(
                "Variable `{$key}` must be in: " . implode(', ', $available) . ''
            );
        }
    }

    /**
     * Test variable for the specified range.
     *
     * @param string $name
     * @param float $value
     * @param float $min
     * @param float $max
     */
    protected function ruleInRange(string $name, float $value, float $min, float $max): void
    {
        if ($value < $min || $value > $max) {
            throw new InvalidArgumentException("Variable `{$name}` must be between {$min} and {$max}!");
        }
    }
}
