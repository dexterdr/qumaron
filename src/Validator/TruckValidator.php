<?php

namespace App\Validator;

final class TruckValidator extends CarValidator
{
    protected const BODY = ['LIGHT', 'MEDIUM', 'HUGE'];
    protected const VOLUME_MIN = 2;
    protected const VOLUME_MAX = 10;
}
