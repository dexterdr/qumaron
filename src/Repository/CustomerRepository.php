<?php

namespace App\Repository;

use App\Entity\Customer;
use App\Interfaces\CustomerInterface;
use InvalidArgumentException;

class CustomerRepository
{
    /**
     * @var CustomerInterface[]
     */
    private $customers = [];

    /**
     * Create repository from request data.
     *
     * @param array<string, mixed> $request
     *
     * @return CustomerRepository
     */
    public static function create(array $request): CustomerRepository
    {
        $customerRepository = new self();

        foreach ($request as $name => $data) {
            try {
                $customer = $customerRepository->addCustomer($name, $data['email'] ?? '');

                if (!isset($data['orders']) || !is_array($data['orders'])) {
                    continue;
                }

                foreach ($data['orders'] as $title => $details) {
                    $customer->addOrder($title, $details);
                }
            } catch (InvalidArgumentException $e) {
                echo $e->getMessage() . "\n";
            }
        }

        return $customerRepository;
    }

    /**
     * Add a new customer.
     *
     * @param string $name
     * @param string $email
     *
     * @return CustomerInterface
     */
    public function addCustomer(string $name, $email): CustomerInterface
    {
        $customer = new Customer($name, $email);

        $this->customers[] = $customer;

        return $customer;
    }

    /**
     * Get all customers.
     *
     * @return CustomerInterface[]
     */
    public function getCustomers(): array
    {
        return $this->customers;
    }
}
