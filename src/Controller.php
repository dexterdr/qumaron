<?php

namespace App;

use App\Entity\Factory\PassengerFactory;
use App\Entity\Factory\TruckFactory;
use App\Entity\Manager;
use App\Repository\CustomerRepository;
use Core\Interfaces\ContainerInterface;

class Controller
{
    /**
     * Entry point for the business logic.
     *
     * @param ContainerInterface $container
     */
    public function execute(ContainerInterface $container): void
    {
        $customerRepository = CustomerRepository::create($container->get('request', []));

        $logger = $container->get('logger');
        $manager = new Manager([
            'TRUCK' => new TruckFactory($logger),
            'PASSENGER' => new PassengerFactory($logger)
        ], $container->get('mailer'));

        foreach ($customerRepository->getCustomers() as $customer) {
            foreach ($manager->processClient($customer) as $car) {
                $car->printCharacteristics();
            }
        }
    }
}
