<?php

namespace App\Entity\Stage;

use App\Entity\Car\AbstractCar;
use App\Interfaces\FactoryInterface;

class TransmissionStage extends AbstractHandler
{
    /**
     * @inheritDoc
     */
    public function handle(array $details, AbstractCar $car, FactoryInterface $factory): AbstractCar
    {
        $car->setTransmission($details['transmission'])
            ->setGearbox($details['gearbox']);

        return parent::handle($details, $car, $factory);
    }
}
