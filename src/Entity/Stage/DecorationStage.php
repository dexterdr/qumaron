<?php

namespace App\Entity\Stage;

use App\Entity\Car\AbstractCar;
use App\Interfaces\FactoryInterface;

class DecorationStage extends AbstractHandler
{
    /**
     * @inheritDoc
     */
    public function handle(array $details, AbstractCar $car, FactoryInterface $factory): AbstractCar
    {
        $car->setInterior($details['interior'])
            ->setOptions($details['options'])
            ->setColor($details['color'])
            ->finish();

        $this->log('Car is fully complete!');

        return parent::handle($details, $car, $factory);
    }
}
