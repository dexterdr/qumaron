<?php

namespace App\Entity\Stage;

use App\Entity\Car\AbstractCar;
use App\Interfaces\FactoryInterface;

class EngineStage extends AbstractHandler
{
    /**
     * @inheritDoc
     */
    public function handle(array $details, AbstractCar $car, FactoryInterface $factory): AbstractCar
    {
        $car->setEngine($factory->createEngine(
            $details['engine'], $details['volume'],
        ));

        $this->log('Engine was installed!');

        return parent::handle($details, $car, $factory);
    }
}
