<?php

namespace App\Entity\Stage;

use App\Entity\Car\AbstractCar;
use App\Interfaces\FactoryInterface;
use Psr\Log\LoggerInterface;

class AbstractHandler
{
    /**
     * @var AbstractHandler
     */
    private $next;

    /**
     * @var LoggerInterface|null
     */
    private $logger;

    /**
     * AbstractHandler constructor.
     *
     * @param LoggerInterface|null $logger
     */
    public function __construct(?LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     * Use this function to build production chain.
     *
     * @param AbstractHandler $next
     *
     * @return AbstractHandler
     */
    public function setNext(AbstractHandler $next): AbstractHandler
    {
        $this->next = $next;

        return $next;
    }

    /**
     * Handle single assembly stage.
     *
     * @param array<string, mixed> $details
     * @param AbstractCar $car
     * @param FactoryInterface $factory
     *
     * @return AbstractCar
     */
    public function handle(array $details, AbstractCar $car, FactoryInterface $factory): AbstractCar
    {
        if ($this->next === null) {
            return $car;
        }

        return $this->next->handle($details, $car, $factory);
    }

    /**
     * Log some information (context or log level is not required for this demo).
     *
     * @param string $message
     */
    protected function log(string $message): void
    {
        if ($this->logger) {
            $this->logger->info($message);
        }
    }
}
