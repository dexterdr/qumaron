<?php

namespace App\Entity\Stage;

use App\Entity\Car\AbstractCar;
use App\Interfaces\FactoryInterface;

class BodyStage extends AbstractHandler
{
    /**
     * @inheritDoc
     */
    public function handle(array $details, AbstractCar $car, FactoryInterface $factory): AbstractCar
    {
        $car->setBody($details['body']);

        $this->log('Car body is complete!');

        return parent::handle($details, $car, $factory);
    }
}
