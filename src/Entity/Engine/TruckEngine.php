<?php

namespace App\Entity\Engine;

final class TruckEngine extends AbstractEngine
{
    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return "Lorry {$this->type} engine";
    }
}
