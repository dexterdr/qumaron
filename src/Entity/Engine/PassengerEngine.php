<?php

namespace App\Entity\Engine;

final class PassengerEngine extends AbstractEngine
{
    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return "Passenger {$this->type} engine";
    }
}
