<?php

namespace App\Entity\Engine;

abstract class AbstractEngine
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var float
     */
    protected $volume;

    /**
     * Engine constructor.
     *
     * @param string $type
     * @param float $volume
     */
    public function __construct(string $type, float $volume)
    {
        $this->type = $type;
        $this->volume = $volume;
    }

    /**
     * Get engine volume.
     *
     * @return float
     */
    public function getVolume(): float
    {
        return $this->volume;
    }

    /**
     * Get engine type (diesel or fuel).
     *
     * @return string
     */
    abstract public function getType(): string;
}
