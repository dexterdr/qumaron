<?php

namespace App\Entity\Car;

use App\Entity\Engine\AbstractEngine;
use RuntimeException;

/**
 * Class AbstractCar
 *
 * I skip getters generation to avoid huge amount of unnecessary code (for this task).
 */
abstract class AbstractCar
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $body;

    /**
     * @var AbstractEngine
     */
    private $engine;

    /**
     * @var string
     */
    private $transmission;

    /**
     * @var int
     */
    private $gearbox;

    /**
     * @var string
     */
    private $interior;

    /**
     * @var array<string>
     */
    private $options = [];

    /**
     * @var string
     */
    private $color;

    /**
     * @var bool
     */
    private $finished = false;

    /**
     * AbstractCar constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $body
     *
     * @return AbstractCar
     */
    public function setBody(string $body): AbstractCar
    {
        if ($this->finished) {
            throw new RuntimeException('This option is now locked for changes');
        }

        $this->body = $body;

        return $this;
    }

    /**
     * @param AbstractEngine $engine
     *
     * @return AbstractCar
     */
    public function setEngine(AbstractEngine $engine): AbstractCar
    {
        $this->engine = $engine;

        return $this;
    }

    /**
     * @param string $transmission
     *
     * @return AbstractCar
     */
    public function setTransmission(string $transmission): AbstractCar
    {
        $this->transmission = $transmission;

        return $this;
    }

    /**
     * @param int $gearbox
     *
     * @return AbstractCar
     */
    public function setGearbox(int $gearbox): AbstractCar
    {
        $this->gearbox = $gearbox;

        return $this;
    }

    /**
     * @param string $interior
     *
     * @return AbstractCar
     */
    public function setInterior(string $interior): AbstractCar
    {
        $this->interior = $interior;

        return $this;
    }

    /**
     * @param array<string> $options
     *
     * @return AbstractCar
     */
    public function setOptions(array $options): AbstractCar
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @param string $color
     *
     * @return AbstractCar
     */
    public function setColor(string $color): AbstractCar
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Lock some changes after assembling.
     */
    public function finish(): void
    {
        $this->finished = true;
    }

    /**
     * Is car finished?
     *
     * @return bool
     */
    public function isFinished(): bool
    {
        return $this->finished;
    }

    /**
     * Display car info.
     */
    public function printCharacteristics(): void
    {
        echo "\n*** {$this->name} *** ({$this->getType()})\n" .
            "Body: {$this->body}\n" .
            "Engine: {$this->engine->getType()} ({$this->engine->getVolume()})\n" .
            "Transmission: {$this->transmission} ({$this->gearbox})\n" .
            "Interior: {$this->interior}\n" .
            'Options: ' . implode(', ', $this->options) . "\n" .
            "Color: {$this->color}\n";
    }

    /**
     * Get car type (just to make a difference).
     *
     * @return string
     */
    abstract public function getType(): string;
}
