<?php

namespace App\Entity\Car;

class PassengerCar extends AbstractCar
{
    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'PASSENGER';
    }
}
