<?php

namespace App\Entity\Car;

class TruckCar extends AbstractCar
{
    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'TRUCK';
    }
}
