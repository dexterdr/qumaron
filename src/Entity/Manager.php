<?php

namespace App\Entity;

use App\Entity\Car\AbstractCar;
use App\Interfaces\CustomerInterface;
use App\Interfaces\FactoryInterface;
use App\Interfaces\ManagerInterface;
use App\Interfaces\OrderInterface;
use Core\Mailer\MailerInterface;
use InvalidArgumentException;
use RuntimeException;

class Manager implements ManagerInterface
{
    /**
     * @var FactoryInterface[]
     */
    private $factories;

    /**
     * @var MailerInterface|null
     */
    private $mailer;

    /**
     * Manager constructor.
     *
     * @param FactoryInterface[] $factories
     * @param MailerInterface|null $mailer
     */
    public function __construct(array $factories = [], ?MailerInterface $mailer = null)
    {
        $this->factories = $factories;
        $this->mailer = $mailer;
    }

    /**
     * Process all client's order.
     *
     * @param CustomerInterface $customer
     *
     * @return AbstractCar[]
     */
    public function processClient(CustomerInterface $customer): array
    {
        $result = [];

        foreach ($customer->getOrders() as $order) {
            try {
                if (!$factory = $this->factories[$order->getType()] ?? null) {
                    throw new InvalidArgumentException('Unknown car type! Skipping...');
                }

                $order->setDetails($factory->getValidator()->validate($order->getDetails()));
                $car = $this->processOrder($order, $factory);

                if (!$car->isFinished()) {
                    throw new RuntimeException('Car is unfinished after assembly, check logs!');
                }

                $this->giveFeedback($customer);
                $result[] = $car;
            } catch (InvalidArgumentException $e) {
                echo $order->getTitle() . ': ' . $e->getMessage() . "\n";
            } catch (RuntimeException $e) {
                echo 'Very strange error: ' . $e->getMessage() . "\n";
            }
        }

        return $result;
    }

    /**
     * Process single order.
     *
     * @param OrderInterface $order
     * @param FactoryInterface $factory
     *
     * @return AbstractCar
     */
    private function processOrder(OrderInterface $order, FactoryInterface $factory): AbstractCar
    {
        $futureCar = $factory->createCar($order->getTitle());

        return $factory->getAssemblyChain()->handle($order->getDetails(), $futureCar, $factory);
    }

    /**
     * Give feedback to a customer.
     *
     * @param CustomerInterface $customer
     */
    private function giveFeedback(CustomerInterface $customer): void
    {
        if (!$this->mailer) {
            return;
        }

        echo 'Sending feedback to a customer... ';

        $result = $this->mailer->send(
            $customer->getEmail(),
            'Your car is ready!',
            'Dear, ' . $customer->getName() . '...');

        echo ($result ? 'Done!' : 'Failed!') . "\n";
    }
}
