<?php

namespace App\Entity;

use App\Interfaces\CustomerInterface;
use App\Interfaces\OrderInterface;
use InvalidArgumentException;

class Customer implements CustomerInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var OrderInterface[]
     */
    private $orders = [];

    /**
     * Client constructor.
     *
     * @param string $name
     * @param string $email
     */
    public function __construct(string $name, $email)
    {
        if (!$name = mb_convert_case(trim($name), MB_CASE_TITLE)) {
            throw new InvalidArgumentException("Customer's name is required!");
        }

        $this->name = $name;
        $this->setEmail($email);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function setEmail($email): void
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException('Invalid email was specified!');
        }

        $this->email = $email;
    }

    /**
     * @inheritDoc
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @inheritDoc
     */
    public function addOrder(string $title, $details): void
    {
        $this->orders[] = new Order($title, $details);
    }

    /**
     * @inheritDoc
     */
    public function getOrders(): array
    {
        return $this->orders;
    }
}
