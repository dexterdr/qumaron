<?php

namespace App\Entity;

use App\Interfaces\OrderInterface;
use InvalidArgumentException;

class Order implements OrderInterface
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array<string, mixed>
     */
    private $details;

    /**
     * Order constructor.
     *
     * @param string $title
     * @param array<string, mixed> $details
     */
    public function __construct(string $title, $details)
    {
        if (!$title = trim($title)) {
            throw new InvalidArgumentException('Future car must have a name!');
        }

        if (!isset($details['type']) || !is_string($details['type']) || empty($details['type'])) {
            throw new InvalidArgumentException("Car type for '{$title}' can not be empty!");
        }

        $this->title = $title;
        $this->type = $details['type'];
        unset($details['type']);
        $this->details = $details;
    }

    /**
     * @inheritDoc
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @inheritDoc
     */
    public function getDetails(): array
    {
        return $this->details;
    }

    /**
     * @inheritDoc
     */
    public function setDetails(array $details): void
    {
        $this->details = $details;
    }
}
