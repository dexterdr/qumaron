<?php

namespace App\Entity\Factory;

use App\Entity\Car\AbstractCar;
use App\Entity\Car\TruckCar;
use App\Entity\Engine\AbstractEngine;
use App\Entity\Engine\TruckEngine;
use App\Validator\TruckValidator;
use Core\Interfaces\ValidatorInterface;

class TruckFactory extends AbstractFactory
{
    /**
     * @inheritDoc
     */
    public function getValidator(): ValidatorInterface
    {
        static $instance;

        if (!$instance) {
            $instance = new TruckValidator();
        }

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function createCar(string $name): AbstractCar
    {
        return new TruckCar($name);
    }

    /**
     * @inheritDoc
     */
    public function createEngine(string $type, float $volume): AbstractEngine
    {
        return new TruckEngine($type, $volume);
    }
}
