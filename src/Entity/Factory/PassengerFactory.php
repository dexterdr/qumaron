<?php

namespace App\Entity\Factory;

use App\Entity\Car\AbstractCar;
use App\Entity\Car\PassengerCar;
use App\Entity\Engine\AbstractEngine;
use App\Entity\Engine\PassengerEngine;
use App\Validator\PassengerValidator;
use Core\Interfaces\ValidatorInterface;

class PassengerFactory extends AbstractFactory
{
    /**
     * @inheritDoc
     */
    public function getValidator(): ValidatorInterface
    {
        static $instance;

        if (!$instance) {
            $instance = new PassengerValidator();
        }

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function createCar(string $name): AbstractCar
    {
        return new PassengerCar($name);
    }

    /**
     * @inheritDoc
     */
    public function createEngine(string $type, float $volume): AbstractEngine
    {
        return new PassengerEngine($type, $volume);
    }
}
