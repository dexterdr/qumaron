<?php

namespace App\Entity\Factory;

use App\Entity\Stage\AbstractHandler;
use App\Entity\Stage\BodyStage;
use App\Entity\Stage\DecorationStage;
use App\Entity\Stage\EngineStage;
use App\Entity\Stage\TransmissionStage;
use App\Interfaces\FactoryInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractFactory implements FactoryInterface
{
    /**
     * @var LoggerInterface|null
     */
    private $logger;

    /**
     * AbstractFactory constructor.
     *
     * @param LoggerInterface|null $logger
     */
    public function __construct(?LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     * Now it equal for all the car types.
     *
     * @inheritDoc
     */
    public function getAssemblyChain(): AbstractHandler
    {
        static $instance;

        if (!$instance) {
            $instance = new BodyStage($this->logger);
            $instance
                ->setNext(new EngineStage($this->logger))
                ->setNext(new TransmissionStage($this->logger))
                ->setNext(new DecorationStage($this->logger));
        }

        return $instance;
    }
}
