<?php

use Core\Application;

require __DIR__ . '/../vendor/autoload.php';

Dotenv\Dotenv::createImmutable(__DIR__ . '/../')->load();

try {
    $app = new Application(__DIR__ . '/' . getenv('REQUEST_SOURCE'));
    $app->run();
} catch (Exception $e) {
    echo 'Unexpected error: ' . $e->getMessage();
}

// echo json_encode(['foo', 'bar', 'baz'], JSON_THROW_ON_ERROR, 512);
