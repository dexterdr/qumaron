<?php

namespace Core\Interfaces;

interface ValidatorInterface
{
    /**
     * Prepare and validate some data.
     *
     * @param array<string, mixed> $data
     *
     * @return array<string, mixed>
     */
    public function validate(array $data): array;
}
