<?php

namespace Core\Interfaces;

/**
 * Interface ContainerInterface
 *
 * I skip has(), delete(), etc. methods to simplify code.
 * Some production-ready DI container like Pimple is more appropriate here.
 */
interface ContainerInterface
{
    /**
     * Get container item.
     *
     * @param string $key
     * @param mixed|null $default
     *
     * @return mixed|null
     */
    public function get(string $key, $default = null);

    /**
     * Set container value.
     *
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, $value): void;
}
