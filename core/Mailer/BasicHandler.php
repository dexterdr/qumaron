<?php

namespace Core\Mailer;

class BasicHandler implements HandlerInterface
{
    /**
     * @inheritDoc
     */
    public function handle(array $request): bool
    {
        $header = "MIME-Version: 1.0\n" .
            "Content-type: text/plain; charset=UTF-8\n";

        return mail(
            $request['to'],
            '=?UTF-8?B?' . base64_encode($request['subject']) . '?=',
            $request['message'],
            $header
        );
    }
}
