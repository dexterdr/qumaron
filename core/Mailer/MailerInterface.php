<?php

namespace Core\Mailer;

/**
 * Interface MailerInterface
 *
 * I don't even try to implement full-functional mailer here.
 * Just a demonstration of classic architecture based on handlers.
 */
interface MailerInterface
{
    /**
     * Set mail handler.
     *
     * @param HandlerInterface $handler
     */
    public function setHandler(HandlerInterface $handler): void;

    /**
     * Send a message.
     *
     * @param string $to
     * @param string $subject
     * @param string $message
     *
     * @return bool
     */
    public function send(string $to, string $subject, string $message): bool;
}
