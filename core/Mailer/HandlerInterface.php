<?php

namespace Core\Mailer;

interface HandlerInterface
{
    /**
     * Handle mail sending request.
     *
     * @param array<string, string> $request
     *
     * @return bool
     */
    public function handle(array $request): bool;
}
