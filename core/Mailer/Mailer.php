<?php

namespace Core\Mailer;

use InvalidArgumentException;
use RuntimeException;

class Mailer implements MailerInterface
{
    /**
     * @var HandlerInterface|null
     */
    private $handler;

    /**
     * Mailer constructor.
     *
     * @param HandlerInterface|null $handler
     */
    public function __construct(?HandlerInterface $handler = null)
    {
        if ($handler) {
            $this->setHandler($handler);
        }
    }

    /**
     * @inheritDoc
     */
    public function setHandler(HandlerInterface $handler): void
    {
        $this->handler = $handler;
    }

    /**
     * @inheritDoc
     */
    public function send(string $to, string $subject, string $message): bool
    {
        if (!$this->handler) {
            throw new RuntimeException('The is no handler specified for this mailer!');
        }

        $subject = trim($subject);
        $message = trim($message);
        if (empty($subject) || empty($message) || !filter_var($to, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException('Invalid email parameters!');
        }

        return $this->handler->handle(compact('to', 'subject', 'message'));
    }
}
