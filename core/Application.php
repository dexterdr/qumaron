<?php

namespace Core;

use App\Controller;
use Core\Interfaces\ContainerInterface;
use Core\Mailer\BasicHandler;
use Core\Mailer\Mailer;
use Monolog\Handler\NativeMailerHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use RuntimeException;

/**
 * Class Application
 *
 * I don't want to turn a test job into a FizzBuzz Enterprise Edition,
 * so I make some simplifications with the output and some other things
 * that the framework should do. Ideally there should be a router,
 * Request and Response interfaces, etc.
 */
class Application
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Application constructor.
     *
     * @param string $filename File with input data in JSON format.
     */
    public function __construct(string $filename)
    {
        if (
            !is_file($filename)
            || (!$json = file_get_contents($filename))
            || (!$request = json_decode($json, true, 512, JSON_THROW_ON_ERROR))
        ) {
            throw new RuntimeException('There is no input data!');
        }

        $this->container = new Container();
        $this->container->set('request', $request);

        $this->setupServices();
    }

    /**
     * Add some services to the application container.
     *
     * I cheat here a little bit, but I really don't want to write own logger with several handlers.
     * Monolog architecture is pretty cool and he has integration plugin for Yii 2 :-)
     */
    private function setupServices(): void
    {
        $log = new Logger('app');

        if ($filename = getenv('LOG_FILE')) {
            $log->pushHandler(new StreamHandler(__DIR__ . '/../log/' . $filename));
        }

        if (($email = getenv('LOG_EMAIL')) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // We may extend this by some additional settings in .env file.
            $log->pushHandler(new NativeMailerHandler($email, 'Log', 'log@app.com', Logger::DEBUG));
        }

        $this->container->set('logger', $log);
        $this->container->set('mailer', new Mailer(new BasicHandler()));
    }

    /**
     * Run our application.
     */
    public function run(): void
    {
        (new Controller())->execute($this->container);
    }
}
