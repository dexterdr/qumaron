<?php

namespace Core;

use Core\Interfaces\ContainerInterface;

class Container implements ContainerInterface
{
    /**
     * @var array<string, mixed>
     */
    private $values = [];

    /**
     * @inheritDoc
     */
    public function get(string $key, $default = null)
    {
        return $this->values[$key] ?? $default;
    }

    /**
     * @inheritDoc
     */
    public function set(string $key, $value): void
    {
        $this->values[$key] = $value;
    }
}
